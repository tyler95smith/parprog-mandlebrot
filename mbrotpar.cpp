
#include <iostream>
#include <mpi.h>
#include <unistd.h>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
//#include "/usr/local/include/mpi.h"
#define MCW MPI_COMM_WORLD
#define PIXELS 8192
#define MAXITER 2048

using namespace std;

void printColor(ostream& fstream,int i){
    int r,g,b;

    //(log(i)/log(MAXITER))*255;
    r=0;
    g=0;
    b=(i%100)*2;
    //if (i==MAXITER) {r=0; g=0; b=0;}
    
    fstream << r << " " << g << " " << b << " ";
}


struct Complex{
    double r;
    double i;
};

Complex operator * (Complex a, Complex b){
    Complex c;
    c.r = a.r*b.r-a.i*b.i;
    c.i = a.r*b.i+a.i*b.r;
    return c;
}

Complex operator + (Complex a, Complex b){
    Complex c;
    c.r = a.r+b.r;
    c.i = a.i+b.i;
    return c;
}

Complex operator - (Complex a, Complex b){
    Complex c;
    c.r = a.r-b.r;
    c.i = a.i-b.i;
    return c;
}

int mbrot_iters(Complex c){
    int i=0;
    Complex z = c;
    while(z.r*z.r+z.i*z.i<2.0*2.0 && i<MAXITER){
        z = z*z+c;
        i++;
    }
    return i;
}


int main(int argc, char **argv){

    int rank, size,pixelCountMin,pixelCountMax;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MCW, &rank); 
    MPI_Comm_size(MCW, &size); 

    double start_time, end_time;

    Complex c1,c2,cx,cdiff;
    double rinc;
    double iinc;
    int iters;
    c1.r = -2.0;
    c1.i = 1.0;

    c2.r = 0.0;
    c2.i = -1.0;

    cdiff = c2-c1;
    rinc = cdiff.r/PIXELS;
    iinc = cdiff.i/PIXELS;

    pixelCountMin = (PIXELS/size)*rank;
    pixelCountMax = pixelCountMin + (PIXELS/size)-1;

//    cout << "rank: " << rank << " pixelCountMin: " << pixelCountMin << " pixelCountMax: " << pixelCountMax << endl;

    stringstream strs;
    strs << "temp/" << rank << "out.txt";
    // store each processes calculations into their own file
    ofstream outfile((strs.str()).c_str()); 

    if (rank == 0)
    {
        start_time = MPI_Wtime();
    }

    for(int i=pixelCountMin;i<=pixelCountMax;++i){
        for(int j=0;j<PIXELS;++j){
            cx.i = c1.i+j*iinc;
            cx.r = c1.r+i*rinc;
            iters = mbrot_iters(cx);        
            printColor(outfile,iters);
        }
    }

    outfile.close();

    // wait for all processes to finish
    MPI_Barrier(MCW); 

    // merge all temp files into one ppm file
    if (rank == 0)
    {
        ofstream outfile("mbrotpar.ppm");

        outfile << "P3" <<endl;
        outfile << PIXELS << " " << PIXELS << endl;
        outfile << "255" <<endl;

        for (int i = 0; i < size; i++)
        {
            stringstream strs;
            strs << "temp/" << i << "out.txt";
            ifstream infile((strs.str()).c_str());
            outfile << infile.rdbuf();
            infile.close();
        }

        outfile.close();


        end_time = MPI_Wtime();

        printf("That took %f seconds\n",end_time-start_time);
    }
/*
    ofstream outfile("mbrot.ppm");


    outfile << "P3" <<endl;
    outfile << PIXELS << " " << PIXELS << endl;
    outfile << "255" <<endl;


    for(int i=0;i<PIXELS;++i){
        for(int j=0;j<PIXELS;++j){
            cx.i = c1.i+j*iinc;
            cx.r = c1.r+i*rinc;
            iters = mbrot_iters(cx);        
            printColor(outfile,iters);
        }
    }
*/ 
    MPI_Finalize();


    return 0;
}

