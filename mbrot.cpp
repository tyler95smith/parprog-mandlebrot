
#include <iostream>
#include <mpi.h>
#include <unistd.h>
#include <cmath>
#include <fstream>
//#include "/usr/local/include/mpi.h"
#define MCW MPI_COMM_WORLD
#define PIXELS 8192
#define MAXITER 1024

using namespace std;

void printColor(ostream& fstream,int i){
    int r,g,b;

    //(log(i)/log(MAXITER))*255;
    r=0;
    g=0;
    b=(i%100)*2;
    //if (i==MAXITER) {r=0; g=0; b=0;}
    
    fstream << r << " " << g << " " << b << " ";
}


struct Complex{
    double r;
    double i;
};

Complex operator * (Complex a, Complex b){
    Complex c;
    c.r = a.r*b.r-a.i*b.i;
    c.i = a.r*b.i+a.i*b.r;
    return c;
}

Complex operator + (Complex a, Complex b){
    Complex c;
    c.r = a.r+b.r;
    c.i = a.i+b.i;
    return c;
}

Complex operator - (Complex a, Complex b){
    Complex c;
    c.r = a.r-b.r;
    c.i = a.i-b.i;
    return c;
}

int mbrot_iters(Complex c){
    int i=0;
    Complex z = c;
    while(z.r*z.r+z.i*z.i<2.0*2.0 && i<MAXITER){
        z = z*z+c;
        i++;
    }
    return i;
}


int main(int argc, char **argv){

    double start_time,end_time;

    Complex c1,c2,cx,cdiff;
    double rinc;
    double iinc;
    int iters;
    c1.r = -2.0;
    c1.i = 1.0;

    c2.r = 0.0;
    c2.i = -1.0;

    cdiff = c2-c1;
    rinc = cdiff.r/PIXELS;
    iinc = cdiff.i/PIXELS;

    ofstream outfile("mbrot.ppm");


    outfile << "P3" <<endl;
    outfile << PIXELS << " " << PIXELS << endl;
    outfile << "255" <<endl;

    start_time = MPI_Wtime();

    for(int i=0;i<PIXELS;++i){
        for(int j=0;j<PIXELS;++j){
            cx.i = c1.i+j*iinc;
            cx.r = c1.r+i*rinc;
            iters = mbrot_iters(cx);        
            printColor(outfile,iters);
        }
    }
    
    end_time = MPI_Wtime();

    printf("That took %f seconds\n",end_time-start_time);

    return 0;
}

